/* See LICENSE file for copyright and license details. */

/* interval between updates (in ms) */
const unsigned int interval = 1000;

/* text to show if no value can be retrieved */
static const char unknown_str[] = "n/a";

/* maximum output string length */
#define MAXLEN 2048

/*
 * function            description                     argument (example)
 *
 * backlight_perc      backlight percentage            device name
 *                                                     (intel_backlight)
 * battery_perc        battery percentage              battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_state       battery charging state          battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_remaining   battery remaining HH:MM         battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * cpu_perc            cpu usage in percent            NULL
 * cpu_freq            cpu frequency in MHz            NULL
 * datetime            date and time                   format string (%F %T)
 * ram_free            free memory in GB               NULL
 * ram_perc            memory usage in percent         NULL
 * ram_total           total memory size in GB         NULL
 * ram_used            used memory in GB               NULL
 * run_command         custom shell command            command (echo foo)
 */
static const struct arg args[] = {
	/* function format          argument */
	//{ datetime, "%s",           "%F %T" },
  //{ netspeed_rx, "%sB/s  ", "enp0s3" },
  { run_command, "%s ", "playerctl metadata --format \" {{ status }}: {{ artist }} - {{ album }} - {{ title }}\"" },
	{ run_command, "[:%4s] ", "echo $(wpctl get-volume @DEFAULT_AUDIO_SINK@ | tail -c 3)%" },
  { backlight_perc, "[ %s] ", "intel_backlight" },
  { battery_perc, "[ %s%%] ", "BAT0" },
	{ cpu_perc, "[ %s%%] ", NULL },
	{ ram_perc, "[ %s%%] ", NULL },
	{ datetime, "%s", "%B %d %T " },
};
